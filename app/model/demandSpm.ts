import { Application } from 'egg';
import { Document } from 'mongoose';
import { IDemandSpm } from '../interface';
import DemandSpmSchema from './schema/demandSpm';

type IDemandSpmModel = IDemandSpm & Document;

export default (app: Application) => {
  const mongoose = app.mongoose;
  const DemandSpm = DemandSpmSchema(app);
  return mongoose.model<IDemandSpmModel>('DemandSpm', DemandSpm);
}

import { Application } from 'egg';

// 定义Schema实例
export default (app: Application) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  /**
   * @param  {String} name 页面名称
   * @param  {String} description 页面描述
   * @param  {String} objectIdType ID
   * @param  {Object} platform 含image_url
   * @param  {Array} extra 额外属性
   * @param  {Date} createdAt 创建时间
   * @param  {Date} updatedAt 更新时间
   */
  const PageSchema = new Schema({
    id: Schema.Types.ObjectId,
    name: { type: String, required: true, index: true },
    description: { type: String },
    objectIdType: { type: String, default: '' },
    platform: { type: Object },
    extra: { type: Array },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() }
  });

  // 设置外键
  PageSchema.virtual('idd', {
    ref: 'Page',
    localField: 'id',
    foreignField: '_id',
    options: { limit: 1 }
  });
  return PageSchema
};

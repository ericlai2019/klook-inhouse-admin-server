import { Application } from 'egg';

export default (app: Application) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const LogSchema = new Schema({
    id: Schema.Types.ObjectId,
    creator: { type: String, default: '' },
    tableId: { type: String, required: true },
    coment: { type: String, required: true },
    createdAt: { type: Date, default: Date.now() }
  });
  return LogSchema
}

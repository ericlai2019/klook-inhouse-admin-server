import { Application } from 'egg';

export default (app: Application) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const DemandSpmSchema = new Schema({
    id: Schema.Types.ObjectId,
    demandId: { type: Schema.Types.ObjectId, required: true, ref: 'Demand' },
    level: { type: String, required: true },
    spmId: { type: Schema.Types.ObjectId, required: true },
    spmName: { type: String, required: true },
    eventType: { type: String, required: true },
    actionType: { type: String, required: false },
    extra: { type: Array, default: [] },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() }
  });

  return DemandSpmSchema
}

import { Application } from 'egg';

// 定义Schema实例
export default (app: Application) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;

  const SpmSchema = new Schema({
    pageId: { type: String, required: true },
    pageName: { type: String, required: true },
    moduleList: { type: Array, default: [] },
    itemList: { type: Array, default: [] }
  });
  return SpmSchema
}

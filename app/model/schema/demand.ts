import { Application } from 'egg';

// 定义Schema实例
export default (app: Application) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  /**
   * @param {String} name 需求名称
   * @param {String} status 需求状态
   * @param {String} description 需求描述
   * @param {Date} createdAt 创建时间
   * @param
   */
  const DemandSchema = new Schema({
    id: Schema.Types.ObjectId,
    name: { type: String, required: true },
    status: { type: String, required: true },
    description: { type: String, required: true },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() }
  });
  return DemandSchema
}

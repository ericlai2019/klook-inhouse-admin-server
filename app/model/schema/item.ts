import { Application } from 'egg';

// 定义Schema实例
export default (app: Application) => {
  const mongoose = app.mongoose;
  const Schema = mongoose.Schema;
  const ItemSchema = new Schema({
    id: Schema.Types.ObjectId,
    pageId: { type: Schema.Types.ObjectId, required: true, ref: 'Page' },
    moduleId: { type: Schema.Types.ObjectId, required: true, ref: 'Module' },
    name: { type: String, required: true, index: true },
    description: { type: String, required: true },
    objectIdType: { type: String, default: '' },
    platform: { type: Object, default: {} },
    extra: { type: Array, default: [] },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() }
  });
  return ItemSchema
}

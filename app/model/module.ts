import { Application } from 'egg';
import { Document } from 'mongoose';
import { IModule } from '../interface';
import ModuleSchema from './schema/module';

type IModuleModel = IModule & Document;

export default (app: Application) => {
  const mongoose = app.mongoose;
  const Module = ModuleSchema(app);
  return mongoose.model<IModuleModel>('Module', Module);
};

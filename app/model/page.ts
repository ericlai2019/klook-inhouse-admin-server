import { Application } from 'egg';
import { Document } from 'mongoose';
import { IPage } from '../interface';
import PageSchema from './schema/page';

// 定义mongoose.Model接口
type IPageModel = IPage & Document;

// 定义Schema实例
export default (app: Application) => {
  const mongoose = app.mongoose;
  const Page = PageSchema(app);
  return mongoose.model<IPageModel>('Page', Page);
};

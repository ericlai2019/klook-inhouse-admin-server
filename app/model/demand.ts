import { Application } from 'egg';
import { Document } from 'mongoose';
import { IDemand } from '../interface';
import DemandSchema from './schema/demand';

// 定义mongoose.Model接口
type IDemandModel = IDemand & Document;

// 定义Schema实例
export default (app: Application) => {
  const mongoose = app.mongoose;
  const Demand = DemandSchema(app);
  return mongoose.model<IDemandModel>('Demand', Demand);
}

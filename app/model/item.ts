import { Application } from 'egg';
import { Document } from 'mongoose';
import { IItem } from '../interface';
import ItemSchema from './schema/item';

type IItemModel = IItem & Document;

export default (app: Application) => {
  const mongoose = app.mongoose;
  const Item = ItemSchema(app);
  return mongoose.model<IItemModel>('Item', Item);
}

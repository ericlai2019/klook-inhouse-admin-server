import { Application } from 'egg';
import { Document } from 'mongoose';
import { ILog } from '../interface';
import LogSchema from './schema/log';

type ILogModel = ILog & Document;

export default (app: Application) => {
  const mongoose = app.mongoose;
  const Log = LogSchema(app);
  return mongoose.model<ILogModel>('Log', Log);
}

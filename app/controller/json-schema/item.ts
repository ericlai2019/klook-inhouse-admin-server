const NAME_MAX_LEN = 50
const DESC_MAX_LEN = 500
export const itemJSONSchema = {
  type: 'object',
  properties: {
    pageId: { type: 'string' },
    moduleId: { type: 'string' },
    name: {
      type: 'string',
      pattern: '^[a-zA-Z][a-zA-Z0-9_-]+',
      maxLength: NAME_MAX_LEN,
      errorMessage: {
        type: '请填写完整',
        maxLength: `最大长度不能超过${NAME_MAX_LEN}个字符！`
      }
    },
    description: {
      type: 'string',
      maxLength: DESC_MAX_LEN,
      errorMessage: {
        type: '请填写完整',
        maxLength: `最大长度不能超过${DESC_MAX_LEN}个`
      }
    },
    objectIdType: { type: 'string' },
    platform: {
      type: 'object',
      items: {
        type: 'object',
        properties: {
          web: {
            type: 'object'
          },
          mweb: {
            type: 'object'
          },
          app: {
            type: 'object'
          }
        }
      }
    },
    extra: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            pattern: '^[a-zA-Z][a-zA-Z0-9_-]*$'
          },
          type: {
            type: [ 'string', 'null' ]
          },
          description: {
            type: [ 'string', 'null' ]
          }
        },
        required: [ 'name', 'type' ]
      }
    }
  },
  required: [ 'pageId', 'moduleId', 'name', 'description', 'objectIdType', 'platform' ],
  additionalProperties: false
}

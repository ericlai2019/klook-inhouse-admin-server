const NAME_MAX_LEN = 100
const DESC_MAX_LEN = 500
export const demandJSONSchema = {
  type: 'object',
  properties: {
    name: {
      type: 'string',
      pattern: '^[a-zA-Z][a-zA-Z0-9_-]+',
      maxLength: NAME_MAX_LEN,
      errorMessage: {
        type: '请填写完整',
        maxLength: `最大长度不能超过${NAME_MAX_LEN}个字符！`
      }
    },
    description: {
      type: 'string',
      maxLength: DESC_MAX_LEN,
      errorMessage: {
        type: '请填写完整',
        maxLength: `最大长度不能超过${DESC_MAX_LEN}个`
      }
    },
    status: { type: 'string' }
  },
  required: [ 'name', 'description', 'status' ],
  additionalProperties: false
}

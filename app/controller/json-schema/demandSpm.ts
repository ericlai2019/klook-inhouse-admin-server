export const demandSpmJSONSchema = {
  type: 'object',
  properties: {
    level: { type: 'string' },
    demandId: { type: 'string' },
    spmId: { type: 'string' },
    spmName: { type: 'string' },
    eventType: { type: 'string' },
    actionType: { type: 'string' },
    extra: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          name: {
            type: 'string',
            pattern: '^[a-zA-Z][a-zA-Z0-9_-]*$'
          },
          type: {
            type: [ 'string', 'null' ]
          },
          description: {
            type: [ 'string', 'null' ]
          }
        },
        required: [ 'name', 'type' ]
      }
    }
  },
  required: [ 'level', 'spmId', 'eventType' ],
  additionalProperties: false
}

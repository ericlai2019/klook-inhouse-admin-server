import { Controller } from 'egg';
import { itemValidate } from './validator/item';
import * as fs from 'fs';
import * as path from 'path';
const sourceDir = path.resolve(__dirname, '../../source/items.json');
// const user = 'eric.lai';
export default class ItemController extends Controller {
  // 创建item
  async create() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const valid = itemValidate(ctx.request.body);
    const errors: any = itemValidate.errors;
    if (!valid) {
      ctx.body = {
        code: 200,
        success: false,
        message: 'item参数不对' + errors[0].dataPath + ':' + errors[0].message
      }
      return
    }
    const itemData = {
      ...ctx.request.body,
      createdAt: +new Date(),
      updatedAt: +new Date()
    }

    const result: any = await ctx.service.item.create(itemData, user);
    ctx.body = result;
  }

  // 查询item
  async get() {
    const { ctx } = this;
    const query = ctx.request.query ? ctx.request.query : {};
    const result: any = await ctx.service.item.get(query);
    ctx.body = result;
  }
  // 获取全部item名称
  async getItemNameList() {
    const { ctx } = this;
    const result: any = await ctx.service.item.getItemNameList();
    ctx.body = result;
  }

  // 根据id获取item的详情
  async getDetail() {
    const { ctx } = this;
    const result: any = await ctx.service.item.getDetail();
    ctx.body = result;
  }
  // 更新item
  async update() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const valid = itemValidate(ctx.request.body);
    const errors: any = itemValidate.errors;
    if (!valid) {
      ctx.body = {
        code: 200,
        success: false,
        message: 'item参数不对' + errors[0].dataPath + ':' + errors[0].message
      }
      return
    }
    const itemData = {
      ...ctx.request.body
    }
    const result: any = await ctx.service.item.update(itemData, user);
    ctx.body = result;
  }

  // 删除item
  async delete() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const result: any = await ctx.service.item.delete('', user);
    ctx.body = result;
  }

  // 批量导入数据
  public async importItems() {
    const { ctx } = this;
    const { Item } = ctx.app.mongoose.models;
    const data = JSON.parse(fs.readFileSync(sourceDir, 'utf8'));
    // 1.校验数据
    const validResult: any[] = [];
    data.forEach(item => {
      const valid: any = itemValidate(item);
      const errors: any = itemValidate.errors;
      if (!valid) {
        validResult.push({
          errMsg: '参数不对' + errors[0].dataPath + ': ' + errors[0].message
        })
      }
    })
    // 导入数据
    if (validResult.length) {
      ctx.body = {
        code: 200,
        success: false,
        message: '数据校验有误',
        result: validResult
      }
    } else {
      const res = await this.insertData(data, Item)
      ctx.body = res
    }
  }
  // 导入数据方法
  public async insertData(data: any, Item: any) {
    for (let i = 0; i < data.length; i++) {
      // 查找数据库存，如果已经存在的数据跳过
      for (let i = 0; i < data.length; i++) {
        const result = await Item.findOne({ name: data[i].name }).exec();
        if (result) {
          continue
        } else {
          await Item.create(data[i])
        }
      }
      return {
        code: 200,
        success: true,
        message: '成功插入数据'
      }
    }
  }
}

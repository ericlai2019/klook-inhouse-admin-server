import { Controller } from 'egg';
import { demandValidate } from './validator/demand';
// const user = 'eric.lai';

export default class DemandController extends Controller {
  // 创建demand
  async create() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const valid: any = demandValidate(ctx.request.body);
    const errors: any = demandValidate.errors;
    if (!valid) {
      ctx.body = {
        code: 200,
        success: false,
        message: '参数不对' + errors[0].dataPath + ': ' + errors[0].message
      }
      return
    }
    const demandData = {
      ...ctx.request.body,
      createdAt: +new Date(),
      updatedAt: +new Date()
    }

    const result: any = await ctx.service.demand.create(demandData, user);
    ctx.body = result;
  }

  async get() {
    const { ctx } = this;
    const query = ctx.request.query ? ctx.request.query : {};
    const result: any = await ctx.service.demand.get(query);
    ctx.body = result;
  }
  // 根据id获取page的详情
  async getDetail() {
    const { ctx } = this;
    const result: any = await ctx.service.demand.getDetail();
    ctx.body = result;
  }

  // 更新需求
  async update() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const valid: any = demandValidate(ctx.request.body);
    const errors: any = demandValidate.errors;
    if (!valid) {
      ctx.body = {
        code: 200,
        success: false,
        message: '参数不对' + errors[0].dataPath + ': ' + errors[0].message
      }
      return
    }
    const demandData = {
      ...ctx.request.body
    }
    const result: any = await ctx.service.demand.update(demandData, user);
    ctx.body = result;
  }

  // 删除需求
  async delete() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const result: any = await ctx.service.demand.delete('', user);
    ctx.body = result;
  }

}

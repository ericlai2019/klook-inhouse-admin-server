import * as Ajv from 'ajv';
import { demandSpmJSONSchema } from '../json-schema/demandSpm';

const ajv = new Ajv({ allErrors: true, jsonPointers: true });
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('ajv-errors')(ajv)

export const demandSpmValidate = ajv.compile(demandSpmJSONSchema);

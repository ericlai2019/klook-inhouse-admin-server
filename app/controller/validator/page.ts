import * as Ajv from 'ajv';
import { pageJSONSchema } from '../json-schema/page';

const ajv = new Ajv({ allErrors: true, jsonPointers: true });
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('ajv-errors')(ajv)
export const pageValidate = ajv.compile(pageJSONSchema);

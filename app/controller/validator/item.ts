import * as Ajv from 'ajv';
import { itemJSONSchema } from '../json-schema/item';

const ajv = new Ajv({ allErrors: true, jsonPointers: true });
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('ajv-errors')(ajv)

export const itemValidate = ajv.compile(itemJSONSchema);

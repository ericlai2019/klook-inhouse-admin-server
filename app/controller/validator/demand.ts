import * as Ajv from 'ajv';
import { demandJSONSchema } from '../json-schema/demand';

const ajv = new Ajv({ allErrors: true, jsonPointers: true });
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('ajv-errors')(ajv)

export const demandValidate = ajv.compile(demandJSONSchema);

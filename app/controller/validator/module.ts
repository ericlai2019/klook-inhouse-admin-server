import * as Ajv from 'ajv';
import { moduleJSONSchema } from '../json-schema/module';

const ajv = new Ajv({ allErrors: true, jsonPointers: true });
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('ajv-errors')(ajv)

export const moduleValidate = ajv.compile(moduleJSONSchema);

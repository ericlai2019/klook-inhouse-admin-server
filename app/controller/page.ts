import { Controller } from 'egg';
import { pageValidate } from './validator/page';
import * as fs from 'fs';
import * as path from 'path';
const sourceDir = path.resolve(__dirname, '../../source/pages.json');
// const user = 'eric.lai'
export default class PageController extends Controller {
  public async ping() {
    this.ctx.body = 'eric';
  }
  // 创建page
  public async create() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const valid: any = pageValidate(ctx.request.body);
    const errors: any = pageValidate.errors;
    if (!valid) {
      ctx.body = {
        code: 200,
        success: false,
        message: '参数不对' + errors[0].dataPath + ': ' + errors[0].message
      }
      return
    }
    const pageData = {
      ...ctx.request.body,
      createdAt: +new Date(),
      updatedAt: +new Date()
    }
    const result: any = await ctx.service.page.create(pageData, user);
    ctx.body = result;
  }

  // 查询page
  public async get() {
    const { ctx } = this;
    const query = ctx.request.query ? ctx.request.query : {};
    const result: any = await ctx.service.page.get(query);
    ctx.body = result;
  }

  // 获取全部page名称
  public async getPageNameList() {
    const { ctx } = this;
    const result: any = await ctx.service.page.getPageNameList();
    ctx.body = result;
  }

  // 根据id获取page的详情
  public async getDetail() {
    const { ctx } = this;
    const result: any = await ctx.service.page.getDetail();
    ctx.body = result;
  }

  // 更新page
  public async update() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const valid = pageValidate(ctx.request.body);
    const errors: any = pageValidate.errors;
    if (!valid) {
      ctx.body = {
        code: 200,
        success: false,
        message: '参数不对' + errors[0].dataPath + ': ' + errors[0].message
      }
      return
    }
    const pageData = {
      ...ctx.request.body
    }
    const result: any = await ctx.service.page.update(pageData, user);
    ctx.body = result;
  }

  // 删除page
  public async delete() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const result: any = await ctx.service.page.delete('', user);
    ctx.body = result
  }

  // 批量导入数据
  public async importPages() {
    const { ctx } = this;
    const { Page } = ctx.app.mongoose.models;
    const data = JSON.parse(fs.readFileSync(sourceDir, 'utf8'));
    // 1.校验数据
    const validResult: any[] = [];
    data.forEach(item => {
      const valid: any = pageValidate(item);
      const errors: any = pageValidate.errors;
      if (!valid) {
        validResult.push({
          errMsg: '参数不对' + errors[0].dataPath + ': ' + errors[0].message
        })
      }
    })
    // 导入数据
    if (validResult.length) {
      ctx.body = {
        code: 200,
        success: false,
        message: '数据校验有误',
        result: validResult
      }
    } else {
      const res = await this.insertData(data, Page)
      ctx.body = res
    }
  }
  // 导入数据方法
  public async insertData(data: any, Page: any) {
    for (let i = 0; i < data.length; i++) {
      // 查找数据库存，如果已经存在的数据跳过
      for (let i = 0; i < data.length; i++) {
        const result = await Page.findOne({ name: data[i].name }).exec();
        if (result) {
          continue
        } else {
          await Page.create(data[i])
        }
      }
      return {
        code: 200,
        success: true,
        message: '成功插入数据'
      }
    }
  }
}


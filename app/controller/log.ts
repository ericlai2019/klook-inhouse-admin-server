import { Controller } from 'egg';

export default class LogController extends Controller {
  async get() {
    const { ctx } = this;
    const query = ctx.request.query ? ctx.request.query : {};
    const result: any = await ctx.service.log.get(query);
    ctx.body = result;
  }
}

import { Controller } from 'egg';
import { moduleValidate } from './validator/module';
import * as fs from 'fs';
import * as path from 'path';
const sourceDir = path.resolve(__dirname, '../../source/modules.json');
// const user = 'eric.lai'
export default class ModuleController extends Controller {
  // 创建module
  async create() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const valid = moduleValidate(ctx.request.body);
    const errors: any = moduleValidate.errors;
    if (!valid) {
      console.log('errors:', errors)
      ctx.body = {
        code: 200,
        success: false,
        message: 'module参数不对' + errors[0].dataPath + ':' + errors[0].message
      }
      return
    }
    const moduleData = {
      ...ctx.request.body,
      createdAt: +new Date(),
      updatedAt: +new Date()
    }
    // const user = ctx.user;
    const result: any = await ctx.service.module.create(moduleData, user);
    ctx.body = result;
  }

  // 查询module
  async get() {
    const { ctx } = this;
    const query = ctx.request.query ? ctx.request.query : {};
    const result: any = await ctx.service.module.get(query);
    ctx.body = result;
  }

  // 获取全部module名称
  async getModuleNameList() {
    const { ctx } = this;
    const result: any = await ctx.service.module.getModuleNameList();
    ctx.body = result;
  }

  // 根据id获取module的详情
  async getDetail() {
    const { ctx } = this;
    const result: any = await ctx.service.module.getDetail();
    ctx.body = result;
  }

  // 更新module
  async update() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const valid = moduleValidate(ctx.request.body);
    const errors: any = moduleValidate.errors;
    if (!valid) {
      ctx.body = {
        code: 200,
        success: false,
        message: 'module参数不对' + errors[0].dataPath + ':' + errors[0].message
      }
      return
    }
    const moduleData = {
      ...ctx.request.body
    }
    const result: any = await ctx.service.module.update(moduleData, user);
    ctx.body = result;
  }

  // 删除module
  async delete() {
    const { ctx } = this;
    // if (!ctx.user.isAdmin) {
    //   ctx.body = {
    //     code: 500,
    //     messgae: '当前用户没有极限'
    //   };
    // }
    const user = ctx.headers.x_token;
    const result: any = await ctx.service.module.delete('', user);
    ctx.body = result;
  }

  // 批量导入数据
  public async importModules() {
    const { ctx } = this;
    const { Module } = ctx.app.mongoose.models;
    const data = JSON.parse(fs.readFileSync(sourceDir, 'utf8'));
    // 1.校验数据
    const validResult: any[] = [];
    data.forEach(item => {
      const valid: any = moduleValidate(item);
      const errors: any = moduleValidate.errors;
      if (!valid) {
        validResult.push({
          errMsg: '参数不对' + errors[0].dataPath + ': ' + errors[0].message
        })
      }
    })
    // 导入数据
    if (validResult.length) {
      ctx.body = {
        code: 200,
        success: false,
        message: '数据校验有误',
        result: validResult
      }
    } else {
      const res = await this.insertData(data, Module)
      ctx.body = res
    }
  }
  // 导入数据方法
  public async insertData(data: any, Module: any) {
    for (let i = 0; i < data.length; i++) {
      // 查找数据库存，如果已经存在的数据跳过
      for (let i = 0; i < data.length; i++) {
        const result = await Module.findOne({ name: data[i].name }).exec();
        if (result) {
          continue
        } else {
          await Module.create(data[i])
        }
      }
      return {
        code: 200,
        success: true,
        message: '成功插入数据'
      }
    }
  }
}

import { Controller } from 'egg';
import { demandSpmValidate } from './validator/demandSpm'
// const user = 'eric.lai';
export default class DemandSpmController extends Controller {
  // create
  async create() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    // 检验参数
    const valid = demandSpmValidate(ctx.request.body);
    const errors: any = demandSpmValidate.errors;
    if (!valid) {
      ctx.body = {
        code: 200,
        success: false,
        message: 'Demand SPM 参数不对' + errors[0].dataPath + ':' + errors[0].message
      }
      return
    }
    const demandSpmData = {
      ...ctx.request.body,
      createdAt: +new Date(),
      updatedAt: +new Date()
    }

    const result: any = await ctx.service.demandSpm.create(demandSpmData, user);
    ctx.body = result;
  }

  async get() {
    const { ctx } = this;
    const query = ctx.request.query ? ctx.request.query : {};
    const result: any = await ctx.service.demandSpm.get(query);
    ctx.body = result;
  }

  // 根据id获取page的详情
  async getDetail() {
    const { ctx } = this;
    const result: any = await ctx.service.demandSpm.getDetail();
    ctx.body = result;
  }

  async getListById() {
    const { ctx } = this;
    const query = ctx.request.query ? ctx.request.query : {};
    const result: any = await ctx.service.demandSpm.getListById(query);
    ctx.body = result;
  }

  async update() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const demandSpmData = {
      ...ctx.request.body
    }
    const result: any = await ctx.service.demandSpm.update(demandSpmData, user);
    ctx.body = result;
  }

  async delete() {
    const { ctx } = this;
    const user = ctx.headers.x_token;
    const result: any = await ctx.service.demandSpm.delete('', user);
    ctx.body = result;
  }
}

// 定义用户接口
export interface IUser {
  createdAt: Date;
  email?: string;
  userName: string;
  password: string;
  isAdmin: boolean;
}


// 定义page埋点的接口
export interface IPage {
  name: string;
  description: string;
  objectIdType: string;
  platform: {};
  extra: Array<any>;
  createdAt: Date;
  updatedAt: Date;
}

// 定义module埋点的接口
export interface IModule {
  pageId: string;
  isConfig: boolean;
  name: string;
  description: string;
  objectIdType: string;
  platform: {};
  extra: Array<any>;
  createdAt: Date;
  updatedAt: Date;
}

// 定义Item埋点的接口
export interface IItem {
  pageId: string;
  moduleId: string;
  name: string;
  description: string;
  objectIdType: string;
  platform: {};
  extra: Array<any>;
  createdAt: Date;
  updatedAt: Date;
}

// 定义Demand需求的接口
export interface IDemand {
  name: string;
  status: string;
  description: string;
  createdAt: Date;
  updatedAt: Date;
}

// 定义Demand需求SPM的接口
export interface IDemandSpm {
  demandId: string;
  level: string;
  spmId: string;
  spmName: string;
  eventType: string;
  actionType: string;
  extra: Array<any>;
  createdAt: Date;
  updatedAt: Date;
}


// 定义spm表关联结构
export interface IModuleSpm {
  moduleName: string;
  moduleId: string;
}

export interface IItemSpm {
  moduleName: string;
  moduleId: string;
  itemName: string;
  itemId: string;
}

export interface ILog {
  creator?: string;
  tableId: string;
  coment: string;
  createdAt: Date;
}

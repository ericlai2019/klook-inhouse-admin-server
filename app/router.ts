import { Application } from 'egg';

export default (app: Application) => {
  const { controller, router, passport } = app;

  router.get('/', controller.home.index);
  // router.get('/register', controller.user.register);
  // router.get('/login', controller.user.login);

  router.get('/inhouse/page/ping', controller.page.ping);
  router.get('/inhouse/page/get', controller.page.get);
  router.get('/inhouse/page/import', controller.page.importPages);
  router.get('/inhouse/page/:id/detail', controller.page.getDetail);
  router.get('/inhouse/page/namelist', controller.page.getPageNameList);
  router.post('/inhouse/page/create', controller.page.create);
  router.put('/inhouse/page/:id/update', controller.page.update);
  router.delete('/inhouse/page/:id/delete', controller.page.delete);

  router.get('/inhouse/module/get', controller.module.get);
  router.get('/inhouse/module/import', controller.module.importModules);
  router.get('/inhouse/module/:id/detail', controller.module.getDetail);
  router.get('/inhouse/module/namelist', controller.module.getModuleNameList);
  router.post('/inhouse/module/create', controller.module.create);
  router.put('/inhouse/module/:id/update', controller.module.update);
  router.delete('/inhouse/module/:id/delete', controller.module.delete);

  router.get('/inhouse/item/get', controller.item.get);
  router.get('/inhouse/item/import', controller.item.importItems);
  router.get('/inhouse/item/:id/detail', controller.item.getDetail);
  router.get('/inhouse/item/namelist', controller.item.getItemNameList);
  router.post('/inhouse/item/create', controller.item.create);
  router.put('/inhouse/item/:id/update', controller.item.update);
  router.delete('/inhouse/item/:id/delete', controller.item.delete);

  router.post('/inhouse/demand/create', controller.demand.create);
  router.get('/inhouse/demand/get', controller.demand.get);
  router.get('/inhouse/demand/:id/detail', controller.demand.getDetail);
  router.put('/inhouse/demand/:id/update', controller.demand.update);
  router.delete('/inhouse/demand/:id/delete', controller.demand.delete);

  router.post('/inhouse/demandspm/create', controller.demandSpm.create);
  router.get('/inhouse/demandspm/get', controller.demandSpm.get);
  router.get('/inhouse/demandspm/:id/detail', controller.demandSpm.getDetail);
  router.get('/inhouse/demandspm/:id/list', controller.demandSpm.getListById);
  router.put('/inhouse/demandspm/:id/update', controller.demandSpm.update);
  router.delete('/inhouse/demandspm/:id/delete', controller.demandSpm.delete);

  router.get('/inhouse/log/get', controller.log.get);

  passport.mount('google', {
    loginURL: '/account/google',
    successMessage: '/'
  });

};

import { Service } from 'egg';
import { IPage } from '../interface';

export default class PageService extends Service {
  // 创建page
  async create(page: IPage, user: any) {
    const { ctx } = this;
    const Page = ctx.model.Page;
    let res: any = {};
    const { name } = page;
    const result = await Page.findOne({ name }).exec()
    if (!result) {
      const newPage = new Page(page);
      const save = await newPage.save()
      res = {
        code: 200,
        success: true,
        message: '创建page埋点成功',
        page: save
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '当前page埋点已存在'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      console.log('record log')
      const { _id, name, description, platform, extra } = res.page;
      const log = {
        creator: user,
        tableId: _id,
        coment: `create: pageName-${name};desc-${description};platform-${platform};extra-${extra.join(',')}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }

  // 查询page
  async get(params?: any) {
    const { ctx } = this;
    const Page = ctx.model.Page;
    let res = {};
    const { page_size = 10, page_num = 1, name = '' } = params;
    const pageSize = parseInt(page_size);
    const pageNum = parseInt(page_num);
    const start = (pageNum - 1) * pageSize;
    // name 按模糊查询
    const reg = new RegExp(name, 'i');
    const baseQuery = this.getBaseQuery({ start, pageSize })
    const query = {
      $match: {
        $or: [
          {
            name: { $regex: reg }
          }
        ]
      }
    }
    const pages = await Page.aggregate([ query, ...baseQuery ])
    const [ { count: totalCount } = { count: 0 } ] = await Page.aggregate([ query, { $count: 'count' }])
    if (pages && pages.length) {
      res = {
        code: 200,
        success: true,
        result: {
          list: pages,
          total: totalCount
        }
      }
    } else {
      res = {
        code: 200,
        success: true,
        result: {
          list: [],
          total: totalCount
        },
        message: '暂无数据'
      }
    }
    return res;
  }
  getBaseQuery(query: any) {
    return [
      { $sort: { createdAt: 1 } },
      { $skip: query.start },
      { $limit: query.pageSize }
    ]
  }
  // 根据id获取page的详情
  async getDetail(pageId?: string) {
    const { ctx } = this;
    const Page = ctx.model.Page;
    let { id = '' } = ctx.params || {};
    let res: any = {}
    id = pageId ? pageId : id;
    // id是否符合数据库的ObjetId.Types. TODO
    const result = await Page.findById(id).exec();
    if (result) {
      res = {
        code: 200,
        success: true,
        page: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '获取详情失败'
      }
    }
    return res;
  }

  // 查询全部的pageName列表
  async getPageNameList() {
    const { ctx } = this;
    const Page = ctx.model.Page;
    let res = {};
    const result = await Page.find({}, { name: 1, extra: 1 }).exec();
    if (result) {
      res = {
        code: 200,
        success: true,
        list: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '没有数据'
      }
    }
    return res;
  }

  // 更新page数据
  async update(updateData: any, user: any, pageId?: any) {
    const { ctx } = this;
    const Page = ctx.model.Page;
    let { id = '' } = ctx.params || {};
    updateData = {
      ...updateData,
      updatedAt: +new Date()
    }
    id = pageId ? pageId : id;
    let res: any = {};
    const result = await Page.findByIdAndUpdate(id, { ...updateData });
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '更新成功',
        page: updateData
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '更新失败'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { name, description, platform, extra } = res.page;
      const log = {
        creator: user,
        tableId: id,
        coment: `update: pageName-${name};desc-${description};platform-${platform};extra-${extra.join(',')}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }

  // 删除page
  async delete(deleteId?: string, user?: any) {
    const { ctx } = this;
    const { Page, Module, Item } = ctx.model;
    let { id = '' } = ctx.params || {};
    let res: any = {};
    id = deleteId ? deleteId : id;
    const result = await Page.findByIdAndDelete(id);
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '删除page成功',
        page: result
      }
    } else {
      res = {
        code: 200,
        success: false,
        message: '删除page失败'
      }
    }

    // 删除page下module
    let moduleList: any [] = [];
    const moduleResult = await Module.find({ pageId: id });
    if (result && moduleResult) {
      moduleList = moduleResult.map(v => v._id)
    }
    if (moduleList.length) {
      moduleList.forEach(async (m: string) => {
        await ctx.service.module.delete(m, user)
      })
    }
    // 同时删除page下的item
    let itemList: any [] = [];
    const itemResult = await Item.find({ pageId: id });
    if (result && itemResult) {
      itemList = itemResult.map(v => v._id)
    }
    if (itemList.length) {
      itemList.forEach(async (v: string) => {
        await ctx.service.item.delete(v, user)
      })
    }
    // 同时记录操作日志
    if (res.success === true) {
      const { name } = res.page;
      const log = {
        creator: user,
        tableId: id,
        coment: `delete: pageName-${name}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }
}

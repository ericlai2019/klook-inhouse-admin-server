import { Service } from 'egg';
import { IDemand } from '../interface';

export default class DemandService extends Service {
  // 创建demand
  async create(demand: IDemand, user?: any) {
    const { ctx } = this;
    const Demand = ctx.model.Demand;
    let res: any = {};
    const { name } = demand;
    const result = await Demand.findOne({ name }).exec();
    if (!result) {
      const newDemand = new Demand(demand);
      const saveDemand = await newDemand.save();
      res = {
        code: 200,
        success: true,
        message: '创建需求成功',
        demand: saveDemand
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '当前需求已存在'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { _id, name, description, status } = res.demand;
      const log = {
        creator: user,
        tableId: _id,
        coment: `create: demandName-${name};desc-${description};status-${status};`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }

  // 查询需求
  async get(params?: any) {
    const { ctx } = this;
    const Demand = ctx.model.Demand;
    let res = {};
    const { page_size = 10, page_num = 1, name = '' } = params;
    const pageSize = parseInt(page_size);
    const pageNum = parseInt(page_num);
    const start = (pageNum - 1) * pageSize;
    const match: any = {};
    name && (match.name = name);
    // name 按模糊查询
    const reg = new RegExp(name, 'i');
    const baseQuery = this.getBaseQuery({ start, pageSize });
    const query = {
      $match: {
        $or: [
          {
            name: { $regex: reg }
          }
        ]
      }
    }
    const demands = await Demand.aggregate([ query, ...baseQuery ]);
    const [ { count } = { count: 0 } ] = await Demand.aggregate([ query, { $count: 'count' }]);
    if (demands && demands.length) {
      res = {
        code: 200,
        success: true,
        result: {
          list: demands,
          total: count
        }
      }
    } else {
      res = {
        code: 200,
        success: true,
        result: {
          list: [],
          total: count
        },
        message: '暂无数据'
      }
    }
    return res;
  }

  getBaseQuery(query: any) {
    return [
      { $sort: { createdAt: 1 } },
      { $skip: query.start },
      { $limit: query.pageSize }
    ]
  }

  // 根据id获取demand的详情
  async getDetail(demandId?: any) {
    const { ctx } = this;
    const Demand = ctx.model.Demand;
    let { id = '' } = ctx.params || {};
    id = demandId ? demandId : id;
    let res: any = {}
    const result = await Demand.findById(id).populate('demandId')
      .exec();
    if (result) {
      res = {
        code: 200,
        success: true,
        demand: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '获取详情失败'
      }
    }
    return res;
  }
  // 更新需求数据
  async update(updateData: any, user: any, demandId?: any) {
    const { ctx } = this;
    const Demand = ctx.model.Demand;
    let { id = '' } = ctx.params || {};
    id = demandId ? demandId : id;
    updateData = {
      ...updateData,
      updatedAt: +new Date()
    }
    let res: any = {};
    const result = await Demand.findByIdAndUpdate(id, { ...updateData });
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '更新成功',
        demand: updateData
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '更新失败'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { name, description } = res.demand;
      const log = {
        creator: user,
        tableId: id,
        coment: `update: demandName-${name};desc-${description};`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }

  // 删除需求
  async delete(deleteId?: string, user?: any) {
    const { ctx } = this;
    const { Demand, DemandSpm } = ctx.model;
    let { id = '' } = ctx.params || {};
    let res: any = {};
    id = deleteId ? deleteId : id;
    const result = await Demand.findByIdAndDelete(id);
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '删除成功',
        demand: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '删除失败'
      }
    }
    // 删除当前需求下的demandspm
    let spmList: any [] = [];
    const spmResult = await DemandSpm.find({ demandId: id });
    if (spmResult) {
      spmList = spmResult.map(v => v._id)
    }
    if (spmList.length) {
      spmList.forEach(async (id: string) => {
        await ctx.service.demandSpm.delete(id, user)
      })
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { name } = res.demand;
      const log = {
        creator: user,
        tableId: id,
        coment: `delete: demandName-${name}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }
}

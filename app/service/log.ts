import { Service } from 'egg';
import { ILog } from '../interface';

export default class LogService extends Service {
  async create(log: ILog) {
    const { ctx } = this;
    const Log = ctx.model.Log;
    const newLog = new Log(log);
    const save = await newLog.save()
    return {
      code: 200,
      success: true,
      message: '创建日志成功',
      log: save
    };
  }

  async get(params?: any) {
    const { ctx } = this;
    const Log = ctx.model.Log;
    let res = {};
    const [ { count } = { count: 0 } ] = await Log.aggregate([{ $count: 'count' }]);
    const { page_size = 10, page_num = 1, tableId = '' } = params;
    const pageSize = parseInt(page_size);
    const pageNum = parseInt(page_num);
    const start = (pageNum - 1) * pageSize;
    const match = tableId ? { tableId } : {};
    const result = await Log.aggregate([
      { $match: match },
      { $skip: start },
      { $limit: pageSize }
    ]);
    if (result && result.length) {
      res = {
        code: 200,
        success: true,
        result: {
          list: result,
          total: count
        }
      }
    } else {
      res = {
        code: 200,
        success: true,
        result: {
          list: [],
          total: count
        },
        message: '暂无数据'
      }
    }
    return res;
  }
}

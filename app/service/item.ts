import { Service } from 'egg';
import { IItem } from '../interface';

export default class ItemService extends Service {
  async createItem(item: any) {
    const { ctx } = this;
    const Item = ctx.model.Item;
    const newItem = new Item(item);
    let res: any = {}
    const saveItem = await newItem.save();
    res = {
      code: 200,
      success: true,
      message: '创建item埋点成功',
      item: saveItem
    }
    return res;
  }
  // 创建item
  async create(item: IItem, user: any) {
    const { ctx } = this;
    const Item = ctx.model.Item;
    let res: any = {};
    const { name, pageId, moduleId } = item;
    const result = await Item.find({ pageId }).exec();
    if (!result) {
      const itemRes = await this.createItem(item);
      res = itemRes;
    } else {
      const itemNameList = result.map(v => `${v.moduleId}.${v.name}`);
      if (itemNameList.includes(`${moduleId}.${name}`)) {
        res = {
          code: 500,
          success: false,
          message: '当前item埋点已经存在'
        }
      } else {
        const itemRes = await this.createItem(item);
        res = itemRes;
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { _id, name, moduleId, pageId, description, platform, extra } = res.item;
      const log = {
        creator: user,
        tableId: _id,
        coment: `create: itemName-${pageId}.${moduleId}.${name};desc-${description};platform-${platform};extra-${extra.join(',')}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    // 同时记录操作日志
    return res
  }

  // 查询item
  async get(params?: any) {
    const { ctx } = this;
    const Item = ctx.model.Item;
    let res = {};
    const { page_size = 10, page_num = 1, name = '', moduleId = '', pageId = '' } = params;
    const pageSize = parseInt(page_size);
    const pageNum = parseInt(page_num);
    const start = (pageNum - 1) * pageSize;
    const match: any = {};
    name && (match.name = name);
    moduleId && (match.moduleId = moduleId);
    pageId && (match.pageId = pageId);
    // name 按模糊查询
    const reg = new RegExp(name, 'i');
    const baseQuery = this.getBaseQuery({ start, pageSize });
    const query = {
      $match: {
        $or: [
          {
            name: { $regex: reg }
          }
        ]
      }
    }
    const items = await Item.aggregate([ query, ...baseQuery ]);
    const [ { count } = { count: 0 } ] = await Item.aggregate([ query, { $count: 'count' }
    ]);
    if (items && items.length) {
      res = {
        code: 200,
        success: true,
        result: {
          list: items,
          total: count
        },
        message: '获取数据成功'
      }
    } else {
      res = {
        code: 200,
        success: true,
        result: {
          list: [],
          total: count
        },
        message: '暂无数据'
      }
    }
    return res;
  }

  // 返回基本查询语句
  getBaseQuery(query: any) {
    return [
      { $sort: { createdAt: 1 } },
      { $skip: query.start },
      { $limit: query.pageSize },
      {
        $lookup: {
          from: 'pages',
          localField: 'pageId',
          foreignField: '_id',
          as: 'pageName'
        }
      },
      {
        $lookup: {
          from: 'modules',
          localField: 'moduleId',
          foreignField: '_id',
          as: 'moduleName'
        }
      }
    ]
  }

  // 查询全部的itemName列表
  async getItemNameList() {
    const { ctx } = this;
    const Item = ctx.model.Item;
    let res = {};
    const result = await Item.find({}, { name: 1, extra: 1 }).populate('pageId', { name: 1 }).populate('moduleId', { name: 1 })
      .exec();
    if (result) {
      const list = result.map((v: any) => {
        return {
          extra: v.extra,
          name: v.name,
          id: v._id,
          moduleName: (v.moduleId && v.moduleId.name) || '',
          moduleId: (v.moduleId && v.moduleId._id) || '',
          pageName: (v.pageId && v.pageId.name) || '',
          pageId: (v.pageId && v.pageId._id) || ''
        }
      })
      res = {
        code: 200,
        success: true,
        list,
        message: '获取数据成功'
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '没有数据'
      }
    }
    return res;
  }

  // 根据id获取item的详情
  async getDetail(itemId?: any) {
    const { ctx } = this;
    const Item = ctx.model.Item;
    let { id = '' } = ctx.params || {};
    id = itemId ? itemId : id;
    let res: any = {}
    const result = await Item.findById(id).populate('pageId', { name: 1 }).populate('moduleId', { name: 1 })
      .exec();
    if (result) {
      res = {
        code: 200,
        success: true,
        item: result,
        message: '获取详情成功'
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '获取详情失败'
      }
    }
    return res;
  }

  // 更新item数据
  async update(updateData: any, user: any, itemId?: any) {
    const { ctx } = this;
    const Item = ctx.model.Item;
    let { id = '' } = ctx.params || {};
    id = itemId ? itemId : id;
    updateData = {
      ...updateData,
      updatedAt: +new Date()
    }
    let res: any = {};
    const result = await Item.findByIdAndUpdate(id, { ...updateData });
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '更新成功',
        item: updateData
      }
    } else {
      res = {
        code: 500,
        success: false,
        messsage: '更新失败'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { name, moduleId, pageId, description, platform, extra } = res.item;
      const log = {
        creator: user,
        tableId: id,
        coment: `update: itemName-${pageId}.${moduleId}.${name};desc-${description};platform-${platform};extra-${extra.join(',')}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }

  // 删除item
  async delete(deleteId: string, user: any) {
    const { ctx } = this;
    const Item = ctx.model.Item;
    let { id = '' } = ctx.params || {};
    let res: any = {};
    id = deleteId ? deleteId : id;
    const result = await Item.findByIdAndDelete(id);
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '删除成功',
        item: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '删除失败'
      }
    }
    // 同时记录操作日志
    if (res.success === true) {
      const { name, moduleId, pageId } = res.item;
      const log = {
        creator: user,
        tableId: id,
        coment: `delete: itemName-${pageId}.${moduleId}.${name};`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }
}


import { Service } from 'egg';
import { IDemandSpm } from '../interface';

export default class DemandSpmService extends Service {
  // 创建
  async create(demandSpm: IDemandSpm, user: any) {
    const { ctx } = this;
    const DemandSpm = ctx.model.DemandSpm;
    let res: any = {};
    const { spmId, demandId } = demandSpm;
    // 查找当前DemandId和SpmId并存条件
    const result = await DemandSpm.findOne({ spmId, demandId }).exec();
    if (!result) {
      const newDemandSpm = new DemandSpm(demandSpm);
      const saveSpm = await newDemandSpm.save();
      res = {
        code: 200,
        success: true,
        message: '创建需求spm成功',
        spm: saveSpm
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '当前需求spm已存在'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { _id, demandId, spmId, level, eventType, platform, extra } = res.spm;
      const log = {
        creator: user,
        tableId: _id,
        coment: `create: spm-demandId-${demandId}.${spmId};level-${level};eventType-${eventType};platform-${platform};extra-${extra.join(',')}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }

  async get(params?: any) {
    const { ctx } = this;
    const DemandSpm = ctx.model.DemandSpm;
    let res = {};
    const { page_size = 10, page_num = 1, spmId = '', name = '' } = params;
    const pageSize = parseInt(page_size);
    const pageNum = parseInt(page_num);
    const start = (pageNum - 1) * pageSize;
    // name 按模糊查询
    const reg = new RegExp(name, 'i');
    const match: any = name ? {
      $or: [
        {
          spmName: { $regex: reg }
        }
      ]
    } : {}
    spmId && (match.spmId = spmId);
    const [ { count } = { count: 0 } ] = await DemandSpm.aggregate([{ $match: match }, { $count: 'count' }]);
    const result = await DemandSpm.aggregate([
      { $match: match },
      {
        $lookup: {
          from: 'demands',
          localField: 'demandId',
          foreignField: '_id',
          as: 'demandName'
        }
      },
      { $sort: { createdAt: -1 } },
      { $skip: start },
      { $limit: pageSize }
    ]);
    if (result && result.length) {
      res = {
        code: 200,
        success: true,
        result: {
          list: result,
          total: count
        }
      }
    } else {
      res = {
        code: 200,
        success: true,
        result: {
          list: [],
          total: count
        },
        message: '暂无数据'
      }
    }
    return res;
  }

  // 根据id获取demandspm详情
  async getDetail(demandSpmId?: any) {
    const { ctx } = this;
    const DemandSpm = ctx.model.DemandSpm;
    let { id = '' } = ctx.params || {};
    id = demandSpmId ? demandSpmId : id;
    let res: any = {};
    const result = await DemandSpm.findById(id);
    if (result) {
      res = {
        code: 200,
        success: true,
        result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '获取详情失败'
      }
    }
    return res;
  }

  // 根据id获取所在列表
  async getListById(params?: any, demandId?: any) {
    const { ctx } = this;
    const DemandSpm = ctx.model.DemandSpm;
    let { id = '' } = ctx.params || {};
    id = demandId ? demandId : id;
    let res: any = {}
    const { page_size = 10, page_num = 1, name = '' } = params;
    const pageSize = parseInt(page_size);
    const pageNum = parseInt(page_num);
    const start = (pageNum - 1) * pageSize;
    // name 按模糊查询
    const reg = new RegExp(name, 'i');
    const baseQuery = this.getBaseQuery({ start, pageSize })
    const listQuery = name ? this.getListQuery(id, reg) : this.getListQuery(id)
    const list = await DemandSpm.aggregate([ ...listQuery, ...baseQuery ]);
    const [ { count } = { count: 0 } ] = await DemandSpm.aggregate([ ...listQuery, { $count: 'count' }])
    if (list && list.length) {
      res = {
        code: 200,
        success: true,
        result: {
          list,
          total: count
        }
      }
    } else {
      res = {
        code: 200,
        success: true,
        result: {
          list: [],
          total: count
        },
        message: '暂无数据'
      }
    }
    return res;
  }

  getBaseQuery(query: any) {
    return [
      { $sort: { createdAt: 1 } },
      { $skip: query.start },
      { $limit: query.pageSize }
    ]
  }

  getListQuery(id: string, reg?: any) {
    const { ctx } = this;
    const mongoose = ctx.app.mongoose;
    const match = reg ?
      {
        $match: {
          demandId: new mongoose.Types.ObjectId(id),
          $or: [
            {
              spmName: { $regex: reg }
            }
          ]
        }
      }
      : {
        $match: { demandId: new mongoose.Types.ObjectId(id) }
      }
    return [
      match,
      {
        $lookup: {
          from: 'demands',
          localField: 'demandId',
          foreignField: '_id',
          as: 'demandName'
        }
      },
      {
        $lookup: {
          from: 'pages',
          localField: 'spmId',
          foreignField: '_id',
          as: 'pageName'
        }
      },
      {
        $lookup: {
          from: 'modules',
          localField: 'spmId',
          foreignField: '_id',
          as: 'moduleName'
        }
      },
      {
        $lookup: {
          from: 'items',
          localField: 'spmId',
          foreignField: '_id',
          as: 'itemName'
        }
      }
    ]
  }

  async update(updateData: any, user: any, demandSpmId?: any) {
    const { ctx } = this;
    const DemandSpm = ctx.model.DemandSpm;
    let { id = '' } = ctx.params || {};
    id = demandSpmId ? demandSpmId : id;
    updateData = {
      ...updateData,
      // id,
      updatedAt: +new Date()
    }
    let res: any = {};
    const result = await DemandSpm.findByIdAndUpdate(id, { ...updateData });
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '更新成功',
        spm: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '更新失败'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { demandId, spmId, level, eventType, platform, extra } = res.spm;
      const log = {
        creator: user,
        tableId: id,
        coment: `update: spm-demandId-${demandId}.${spmId};level-${level};eventType-${eventType};platform-${platform};extra-${extra.join(',')}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }

  async delete(deleteId?: string, user?: any) {
    const { ctx } = this;
    const DemandSpm = ctx.model.DemandSpm;
    let { id = '' } = ctx.params || {};
    let res: any = {};
    id = deleteId ? deleteId : id;
    const result = await DemandSpm.findByIdAndDelete(id);
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '删除成功',
        spm: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '删除失败'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { demandId, spmId } = res.spm;
      const log = {
        creator: user,
        tableId: id,
        coment: `delete: spm-demandId-${demandId}.spmId-${spmId};`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }
}

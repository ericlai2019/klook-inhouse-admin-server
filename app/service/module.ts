import { Service } from 'egg';
import { IModule } from '../interface';

export default class ModuleService extends Service {
  async createModule(module: any) {
    const { ctx } = this;
    const Module = ctx.model.Module;
    const newModule = new Module(module);
    let res: any = {}
    const saveModule = await newModule.save();
    res = {
      code: 200,
      success: true,
      message: '创建module埋点成功',
      module: saveModule
    }
    return res;
  }

  // 创建module
  async create(module: IModule, user: any) {
    const { ctx } = this;
    const Module = ctx.model.Module;
    let res: any = {};
    const { name, pageId } = module;
    // 先用找出当前page的所有module, 再进行是否存在当前新建的module
    const result = await Module.find({ pageId }).exec();
    if (!result) {
      const result = await this.createModule(module);
      res = result;
    } else {
      const moduleNameList = result.map(v => v.name);
      if (moduleNameList.includes(name)) {
        res = {
          code: 500,
          success: false,
          message: '当前module埋点已存在'
        }
      } else {
        const result = await this.createModule(module);
        res = result;
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { _id, name, pageId, description, platform, extra } = res.module;
      const log = {
        creator: user,
        tableId: _id,
        coment: `create: moduleName-${pageId}.${name};desc-${description};platform-${platform};extra-${extra.join(',')}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res
  }

  // 查询module
  async get(params?: any) {
    const { ctx } = this;
    const { Module } = ctx.model;
    let res = {};
    const { page_size = 10, page_num = 1, name = '', id = '', pageId = '' } = params;
    const pageSize = parseInt(page_size);
    const pageNum = parseInt(page_num);
    const start = (pageNum - 1) * pageSize;
    const match: any = {};
    name && (match.name = name);
    id && (match._id = id);
    pageId && (match.pageId = pageId);
    // name 按模糊查询
    if (name.length > 30) {
      return {
        code: 200,
        success: false,
        result: {
          list: []
        },
        message: '请求参数过长'
      }
    }
    const reg = new RegExp(name, 'i');
    const baseQuery = this.getBaseQuery({ start, pageSize })
    const query = {
      $match: {
        $or: [
          {
            name: { $regex: reg }
          }
        ]
      }
    }
    const modules = await Module.aggregate([ query, ...baseQuery ]);
    const [ { count } = { count: 0 } ] = await Module.aggregate([ query, { $count: 'count' }])
    if (modules && modules.length) {
      res = {
        code: 200,
        success: true,
        result: {
          list: modules,
          total: count
        }
      }
    } else {
      res = {
        code: 200,
        success: true,
        result: {
          list: [],
          total: count
        },
        message: '暂无数据'
      }
    }
    return res;
  }

  getBaseQuery(query: any) {
    return [
      { $sort: { createdAt: 1 } },
      { $skip: query.start },
      { $limit: query.pageSize },
      {
        $lookup: {
          from: 'pages',
          localField: 'pageId',
          foreignField: '_id',
          as: 'pageName'
        }
      }
    ]
  }

  // 查询全部的moduleName列表
  async getModuleNameList() {
    const { ctx } = this;
    const Module = ctx.model.Module;
    let res = {};
    const result = await Module.find({}, { name: 1, extra: 1, pageId: 1 }).populate('pageId', { name: 1 }).exec();
    if (result) {
      const list = result.map((v: any) => {
        return {
          extra: v.extra,
          name: v.name,
          id: v._id,
          pageName: (v.pageId && v.pageId.name) || '',
          pageId: (v.pageId && v.pageId._id) || ''
        }
      })
      res = {
        code: 200,
        success: true,
        list
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '没有数据'
      }
    }
    return res;
  }

  // 根据id获取module的详情
  async getDetail(moduleId?: any) {
    const { ctx } = this;
    const Module = ctx.model.Module;
    let { id = '' } = ctx.params || {};
    id = moduleId ? moduleId : id;
    let res: any = {}
    const result = await Module.findById(id).populate('pageId', { name: 1 }).exec();
    if (result) {
      res = {
        code: 200,
        success: true,
        module: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '获取详情失败'
      }
    }
    return res;
  }

  // 更新module数据
  async update(updateData: any, user?: any, moduleId?: any) {
    const { ctx } = this;
    const Module = ctx.model.Module;
    let { id = '' } = ctx.params || {};
    id = moduleId ? moduleId : id;
    updateData = {
      ...updateData,
      updatedAt: +new Date()
    }
    let res: any = {};
    const result = await Module.findByIdAndUpdate(id, { ...updateData });
    console.log('update result:', result)
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '更新成功',
        module: updateData
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '更新失败'
      }
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { name, pageId, description, platform, extra } = res.module;
      const log = {
        creator: user,
        tableId: id,
        coment: `update: moduleName-${pageId}.${name};desc-${description};platform-${platform};extra-${extra.join(',')}`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }

  // 删除module
  async delete(deleteId?: string, user?: any) {
    const { ctx } = this;
    const Module = ctx.model.Module;
    const Item = ctx.model.Item;
    let { id = '' } = ctx.params || {};
    let res: any = {};
    id = deleteId ? deleteId : id;
    const result = await Module.findByIdAndDelete(id);
    if (result) {
      res = {
        code: 200,
        success: true,
        message: '删除成功',
        module: result
      }
    } else {
      res = {
        code: 500,
        success: false,
        message: '删除失败'
      }
    }
    // 同时删除module下的item
    let itemList: any [] = [];
    const itemResult = await Item.find({ moduleId: id });
    if (result && itemResult) {
      itemList = itemResult.map(v => v._id)
    }
    if (itemList.length) {
      itemList.forEach(async (v: string) => {
        await ctx.service.item.delete(v, user)
      })
    }
    // 同时记录操作日志
    if (res.code === 200) {
      const { name, pageId } = res.module;
      const log = {
        creator: user,
        tableId: id,
        coment: `delete: moduleName-${pageId}.${name};`,
        createdAt: new Date()
      }
      await this.app.model.Log.create(log);
    }
    return res;
  }
}

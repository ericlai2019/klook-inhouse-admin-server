import * as assert from 'assert';
import { Context } from 'egg';
import { app } from 'egg-mock/bootstrap';
import { mockAlphabet } from '../../lib/mock';

describe('test/app/service/module.test.js', () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });

  it('创建module埋点', async () => {
    const user = 'klook'
    const data = {
      pageId: '5f670638fb4540ccbbd87f20',
      isConfig: false,
      name: 'new module' + mockAlphabet(),
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const response1: any = await ctx.service.module.create(data, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.module.create(data, user)
    assert(response2.success === false)
  })

  it('更新module埋点', async () => {
    const id = '5f66e8407e9d39a8d713dff3';
    const user = 'klook'
    const data = {
      pageId: '5f66e69cbf153ca27f7d82c0',
      isConfig: false,
      name: 'liuting',
      description: 'alfgalll',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }

    const response1: any = await ctx.service.module.update(data, user, id)
    // console.log('response1:', response1)
    assert(response1.success === true)
  })

  it('删除module埋点成功', async () => {
    const id = '5f670af5bf1cadd7d7d5e29b'
    const user = 'klook'
    const response1: any = await ctx.service.module.delete(id, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.module.delete(id, user)
    assert(response2.success === false)
  })

  it('删除module埋点失败', async () => {
    const id = '5f59a7d0c0e4c33622270239'
    const user = 'klook'
    const response1: any = await ctx.service.module.delete(id, user)
    assert(response1.success === false)
  })


  it('获取module列表', async () => {
    const response1: any = await ctx.service.module.get({})
    // console.log('response1:', response1)
    assert(response1.success === true)
  })

  it('模糊查询module', async () => {
    const query1: any = await ctx.service.module.get({ name: 'tes' })
    // console.log('query1:', query1.result)
    assert(query1.success === true)
  })

  it('获取module name列表', async () => {
    const response1: any = await ctx.service.module.getModuleNameList()
    // console.log('response1:', response1)
    assert(response1.success === true)
  })

  it('根据id获取module详情', async () => {
    const id = '5f66e61b0546519f4755b0da'
    const response1: any = await ctx.service.module.getDetail(id)
    assert(response1.success === true)
  })

});

import * as assert from 'assert';
import { Context } from 'egg';
import { app } from 'egg-mock/bootstrap';
import { mockAlphabet } from '../../lib/mock';

describe('test/app/service/item.test.js', () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });

  after(() => {
    app.close();
  });

  it('校验创建item埋点方法成功', async () => {
    const data = {
      pageId: '5f66e6fce1fa31a33d8c7f61',
      moduleId: '5f66e835a04932a83efbc38a',
      name: 'navitem',
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const response1: any = await ctx.service.item.createItem(data)
    const hasItem: any = await ctx.model.Item.find({ pageId: data.pageId })
    assert(hasItem.name === data.name)
    assert(response1.success === true)
  })


  it('创建item埋点成功', async () => {
    const user = 'klook'
    const data = {
      pageId: '5f62c3862f86a968711a3e6e',
      moduleId: '5f62c555b0b3786b4d6eceef',
      name: 'item' + mockAlphabet(),
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const response1: any = await ctx.service.item.create(data, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.item.create(data, user)
    assert(response2.success === false)
  })

  it('创建item埋点失败', async () => {
    const user = 'klook'
    const data = {
      pageId: '5f62c3862f86a968711a3e6e',
      moduleId: '5f62c555b0b3786b4d6eceef',
      name: 'newfffkdhtm2myn85a',
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      other: 'err'
    }

    const response2: any = await ctx.service.item.create(data, user)
    assert(response2.success === false)
  })

  it('更新item埋点', async () => {
    const id = '5f66e87edaf90eaa372df535';
    const user = 'klook'
    const data = {
      pageId: '5f61d5dee7318c7cfb0397c4',
      moduleId: '5f6206710c7e4e27a56f30fb',
      isConfig: false,
      name: 'limei',
      description: 'alfgalll',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }
    const response1: any = await ctx.service.item.update(data, user, id)
    assert(response1.success === true)
  })

  it('更新item埋点失败', async () => {
    const id = '5f6210031d4c8f3f7ea1be0f';
    const user = 'klook'
    const data = {
      pageId: '5f66d2e218e79684f562ed2a',
      moduleId: '5f6206710c7e4e27a56f30fb',
      isConfig: false,
      name: 'limei',
      description: 'alfgalll',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      aother: 'aflkaa'
    }
    const response1: any = await ctx.service.item.update(data, user, id)
    assert(response1.success === false)
  })

  it('删除item埋点', async () => {
    const id = '5f670d2b3dbf2bdf17dd039b'
    const user = 'klook'
    const response1: any = await ctx.service.item.delete(id, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.item.delete(id, user)
    assert(response2.success === false)
  })


  it('获取item列表', async () => {
    const response1: any = await ctx.service.item.get({})
    assert(response1.success === true)
  })

  it('模糊查询item', async () => {
    const query1: any = await ctx.service.item.get({ name: 'l' })
    assert(query1.success === true)
  })

  it('获取item name列表', async () => {
    const response1: any = await ctx.service.item.getItemNameList()
    assert(typeof response1 === 'object')
    assert(response1.success === true)
  })

  it('根据id获取item详情成功', async () => {
    const id = '5f66e8e8dd5d86ad78c1ffb3'
    const response1: any = await ctx.service.item.getDetail(id)
    assert(response1.success === true)
  })

});

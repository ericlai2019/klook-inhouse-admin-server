import * as assert from 'assert';
import { Context } from 'egg';
import { app } from 'egg-mock/bootstrap';

describe('test/app/service/demandSpm.test.js', () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });
  it('获取demandSpm列表', async () => {
    const response1: any = await ctx.service.demandSpm.get({})
    // console.log('response1:', response1)
    assert(response1.success === true)
  })

  it('模糊查询demandSpm', async () => {
    const query1: any = await ctx.service.demandSpm.get({ name: 'l' })
    console.log(query1.result)
    assert(query1.success === true)
  })


  it('根据id获取demandSpm详情', async () => {
    const id = '5f670288e5fe1bc0b2ba7437'
    const response1: any = await ctx.service.demandSpm.getDetail(id)
    assert(response1.success === true)
  })

  it('根据demandId查找demandSpm埋点', async () => {
    const demandId = '5f66eb8505fc51b49130cc6e'
    const response1: any = await ctx.service.demandSpm.getListById({}, demandId)
    // console.log('response1:', response1.result.list)
    assert(response1.success === true)
  })

  it('创建demandSpm埋点成功', async () => {
    const user = 'klook'
    const data = {
      demandId: '5f670d49107a98dfd4692d20',
      level: 'page',
      spmName: 'eric',
      spmId: '5f66e61b0546519f4755b0dc',
      eventType: 'action',
      actionType: 'click',
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const response1: any = await ctx.service.demandSpm.create(data, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.demandSpm.create(data, user)
    assert(response2.success === false)
  })

  it('创建demandSpm埋点失败', async () => {
    const user = 'klook'
    const data = {
      demandId: '5f66eb8505fc51b49130cc6e',
      level: 'module',
      spmName: 'eric',
      spmId: '5f66e61b0546519f4755b0da',
      eventType: 'action',
      actionType: 'click',
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const response1: any = await ctx.service.demandSpm.create(data, user)
    assert(response1.success === false)

  })

  it('更新demandSpm埋点', async () => {
    const id = '5f67033f9618c3c1b9354418';
    const user = 'kllok'
    const data = {
      demandId: '5f66eca5c89114b850ddbc47',
      level: 'page',
      spmName: 'login',
      spmId: '5f66e61b0546519f4755b0da',
      eventType: 'action',
      actionType: 'click11',
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const response1: any = await ctx.service.demandSpm.update(data, user, id)
    assert(response1.success === true)
  })

  it('删除demandSpm埋点成功', async () => {
    const id = '5f670f44422a05e45d13a5cc'
    const user = 'klook'
    const response1: any = await ctx.service.demandSpm.delete(id, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.demandSpm.delete(id, user)
    assert(response2.success === false)
  })


});

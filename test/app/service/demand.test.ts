import * as assert from 'assert';
import { Context } from 'egg';
import { app } from 'egg-mock/bootstrap';
import { mockAlphabet } from '../../lib/mock';

describe('test/app/service/demand.test.js', () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });

  it('创建demand埋点成功', async () => {
    const user = 'klook'
    const data = {
      name: 'demand' + mockAlphabet(),
      description: 'desc',
      status: 'pending',
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const response1: any = await ctx.service.demand.create(data, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.demand.create(data, user)
    assert(response2.success === false)
  })

  it('创建demand埋点失败', async () => {
    const user = 'klook'
    const data = {
      name: 'login',
      description: 'desc',
      status: 'pending',
      createdAt: new Date(),
      updatedAt: new Date(),
      other: 'afa'
    }
    const response1: any = await ctx.service.demand.create(data, user)
    assert(response1.success === false)
  })

  it('更新demand埋点', async () => {
    const id = '5f66ecc6b3a246b99dfd509b';
    const user = 'klook'
    const data = {
      name: 'eric',
      description: 'alfgalll',
      objectIdType: 'NA',
      status: 'testing'
    }

    const response1: any = await ctx.service.demand.update(data, user, id)
    assert(response1.success === true)
  })

  it('删除demand埋点成功', async () => {
    const id = '5f670e4a388885e2b540eb85'
    const user = 'klook'
    const response1: any = await ctx.service.demand.delete(id, user)
    assert(response1.success === true)
    // const response2: any = await ctx.service.demand.delete(id, user)
    // assert(response2.success === false)
  })

  it('获取demand列表', async () => {
    const response1: any = await ctx.service.demand.get({})
    // console.log('response1:', response1)
    assert(response1.success === true)
  })

  it('模糊查询demand', async () => {
    const query1: any = await ctx.service.demand.get({ name: 'n' })
    console.log(query1.result.list)
    assert(query1.success === true)
  })

  // it('获取demand name列表', async () => {
  //   const response1: any = await ctx.service.demand.getDemandNameList()
  //   console.log('response1:', response1)
  //   assert(response1.success === true)
  // })

  it('根据id获取demand详情', async () => {
    const id = '5f62d38be956d68c3e1b2979'
    const response1: any = await ctx.service.demand.getDetail(id)
    // console.log('response1:', response1)
    assert(response1.success === false)
  })

});

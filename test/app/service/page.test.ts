import * as assert from 'assert';
import { Context } from 'egg';
import { app } from 'egg-mock/bootstrap';
import { mockAlphabet } from '../../lib/mock'

describe('test/app/service/page.test.js', () => {
  let ctx: Context;

  before(async () => {
    ctx = app.mockContext();
  });

  it('创建page埋点成功', async () => {
    const user = 'klook'
    const data = {
      name: 'page' + mockAlphabet(),
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const response1: any = await ctx.service.page.create(data, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.page.create(data, user)
    assert(response2.success === false)
  })

  it('创建page埋点失败', async () => {
    const user = 'klook'
    const data = {
      name: 'homeac',
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      other: 'errr',
      falfalf: 'aflafl'
    }

    const response1: any = await ctx.service.page.create(data, user)
    assert(response1.success === false)

  })

  it('更新page埋点', async () => {
    const id = '5f66dfe66fb756935311e355';
    const user = 'klook'
    const data = {
      name: 'homeac',
      description: 'alfgalll',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }

    const response1: any = await ctx.service.page.update(data, user, id)
    assert(response1.success === true)
  })

  it('更新page埋点失败', async () => {
    const id = '5f66c9d3621f317ccac6982c';
    const user = 'klook'
    const data = {
      name: 'home',
      description: 'alfgalll',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      other: 'err'
    }

    const response1: any = await ctx.service.page.update(data, user, id)
    assert(response1.success === false)
  })

  it('删除page埋点成功', async () => {
    const id = '5f6709b44b49fad453f77089'
    const user = 'klook'
    const response1: any = await ctx.service.page.delete(id, user)
    assert(response1.success === true)

    const response2: any = await ctx.service.page.delete(id, user)
    assert(response2.success === false)
  })

  it('删除page埋点失败', async () => {
    const id = '5f573f3f477c619ab11f46b4'
    const user = 'klook'
    const response2: any = await ctx.service.page.delete(id, user)
    assert(response2.success === false)
  })


  it('获取page列表', async () => {
    const response1: any = await ctx.service.page.get({})
    // console.log('response1:', response1)
    assert(response1.success === true)
  })

  it('模糊查询page', async () => {
    const query1: any = await ctx.service.page.get({ name: 'h' })
    assert(query1.success === true)
  })

  it('获取page name列表', async () => {
    const response1: any = await ctx.service.page.getPageNameList()
    assert(response1.success === true)
  })

  it('根据id获取page详情成功', async () => {
    const id = '5f66e00320b8ee93f4d308de'
    const response1: any = await ctx.service.page.getDetail(id)
    assert(response1.success === true)
  })

  it('根据id获取page详情失败', async () => {
    const id = '5f573dc7ef048898e5f134ca'
    const response1: any = await ctx.service.page.getDetail(id)
    assert(response1.success === false)
  })

});

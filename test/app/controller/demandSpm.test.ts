import * as assert from 'assert';
import { app } from 'egg-mock/bootstrap';

describe('test/app/controller/demandSpm.test.ts', () => {

  it('获取demandSpm列表', async () => {
    const result = await app.httpRequest().get('/inhouse/demandspm/get').expect(200);
    const { body } = result
    // console.log('body:', body.result.list)
    assert(body.success === true);
  }).timeout(6000);


  it('模糊查询demandSpm', async () => {
    const result = await app.httpRequest().get('/inhouse/demandspm/get?name=e').expect(200);
    const { body } = result
    console.log('body:', body.result.list)
    assert(body.success === true);
  }).timeout(6000);

  it('创建demandSpm埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const demandSpm = {
      demandId: '5f66ecbea90abab9023eb105',
      level: 'page',
      spmName: 'new page',
      spmId: '5f66e61b0546519f4755b0dc',
      eventType: 'custom',
      actionType: 'click',
      extra: []
    }
    // 1.创建新demandSpm
    const result1: any = await app.httpRequest().post('/inhouse/demandspm/create')
      .send(demandSpm)
    assert(result1.body.success === true);

    // 2.判断是否可以重复创建
    const result2: any = await app.httpRequest().post('/inhouse/demandspm/create')
      .send(demandSpm)
    assert(result2.body.success === false);

  }).timeout(6000);

  it('创建demandSpm埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const demandSpm = {
      demandId: '5f66ecc6b3a246b99dfd509b',
      level: 'item',
      spmName: 'nnal',
      spmId: '5f66e87edaf90eaa372df535',
      eventType: 'custom',
      actionType: 'click',
      extra: [],
      other: 'eric'
    }
    // 1.创建新demandSpm
    const result1: any = await app.httpRequest().post('/inhouse/demandspm/create')
      .send(demandSpm)
    assert(result1.body.success === false);

  }).timeout(6000);

  it('更新demandSpm埋点', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const demandSpm = {
      demandId: '5f66eca5c89114b850ddbc47',
      level: 'page',
      spmName: 'login',
      spmId: '5f66e61b0546519f4755b0da',
      eventType: 'pageview',
      actionType: '',
      extra: []
    }
    // 更新demandSpm
    const id = '5f67033f9618c3c1b9354418'
    // 更新demandSpm
    const result1: any = await app.httpRequest().put(`/inhouse/demandspm/${id}/update`)
      .send(demandSpm)
    assert(result1.body.success === true);
  });

  it('更新demandSpm埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const demandSpm = {
      demandId: '5f630b26331a2cdbba602306',
      level: 'page',
      spmName: 'login',
      spmId: '5f62c3862f86a968711a3e6e',
      eventType: 'pageview',
      actionType: '',
      extra: [],
      other: 'eric'
    }
    // 更新demandSpm
    const id = '5f62e2faadf173a8166887fb'
    // 更新demandSpm
    const result1: any = await app.httpRequest().put(`/inhouse/demandspm/${id}/update`)
      .send(demandSpm)
    assert(result1.body.success === false);
  })

  it('根据id删除demandSpm埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const id = '5f670569232d3dc79b8c7a98';
    // 删除demandSpm
    const result1: any = await app.httpRequest().delete(`/inhouse/demandspm/${id}/delete`)
    assert(result1.body.success === true);
  });

  it('根据demandId找demandSpm埋点', async () => {
    // app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66eb8505fc51b49130cc6e';
    const result2 = await app.httpRequest().get(`/inhouse/demandspm/${id}/list`);
    console.log('result2.body:', result2.body.result)
    assert(result2.body.success === true)
  });

  it('根据id找demandSpm埋点', async () => {
    // app.mockCsrf(); // 如果不加会报403错误
    const id = '5f670288e5fe1bc0b2ba7437'; // [已经删除]
    const result2 = await app.httpRequest().get(`/inhouse/demandspm/${id}/detail`);
    assert(result2.body.success === true)
  });

});

import * as assert from 'assert';
import { app } from 'egg-mock/bootstrap';
import { mockAlphabet } from '../../lib/mock'

describe('test/app/controller/module.test.ts', () => {

  it('获取module列表', async () => {
    const result = await app.httpRequest().get('/inhouse/module/get').expect(200);
    const { body } = result
    assert(body.code === 200);
  }).timeout(6000);

  it('获取module名称-列表', async () => {
    const result = await app.httpRequest().get('/inhouse/module/namelist').expect(200);
    const { body } = result
    assert(body.code === 200);
  }).timeout(6000);

  it('模糊查询module', async () => {
    const result = await app.httpRequest().get('/inhouse/module/get?name=nav').expect(200);
    const { body } = result
    // console.log('body:', body.result.list)
    assert(body.code === 200);
  }).timeout(6000);

  it('创建module埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const module = {
      pageId: '5f66e681482500a1dbe78b39',
      name: 'new module' + mockAlphabet(),
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }
    // 1.创建新module
    const result1: any = await app.httpRequest().post('/inhouse/module/create')
      .send(module)
    assert(result1.body.success === true);

    // 2.判断是否可以重复创建
    const result2: any = await app.httpRequest().post('/inhouse/module/create')
      .send(module)
    assert(result2.body.success === false);

  }).timeout(6000);

  it('创建module埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const module = {
      pageId: '5f66e5e4894c179e89e7e745',
      name: 'new module1',
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      exa: 'all'
    }
    // 1.创建新module
    const result1: any = await app.httpRequest().post('/inhouse/module/create')
      .send(module)
    assert(result1.body.success === false);

  }).timeout(6000);

  it('更新module埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const module = {
      pageId: '5f66d2e218e79684f562ed2a',
      name: 'eric',
      description: 'update desc',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }
    // 更新module
    const id = '5f66e61b0546519f4755b0da'
    // 更新module
    const result1: any = await app.httpRequest().put(`/inhouse/module/${id}/update`)
      .send(module)
    assert(result1.body.success === true);
  });

  it('更新module埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const module = {
      pageId: '5f66e04e8a81009538b71a7c',
      name: 'eric',
      description: 'update desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      other: 'erro'
    }
    // 更新module
    const id = '5f61e40eb17b0d16c498e755'
    // 更新module
    const result1: any = await app.httpRequest().put(`/inhouse/module/${id}/update`)
      .send(module)
    assert(result1.body.success === false);
  });

  it('根据id删除module埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66e8407e9d39a8d713dff3'; // [已经删除]
    // 删除module
    const result1: any = await app.httpRequest().delete(`/inhouse/module/${id}/delete`)
    // console.log('result1:', result1.body)
    assert(result1.body.success === true);
  });

  it('根据id找module埋点', async () => {
    // app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66e64068ebea9fedcbbbfe'; // [已经删除]
    const result2 = await app.httpRequest().get(`/inhouse/module/${id}/detail`);
    assert(result2.body.success === true)
  });

});

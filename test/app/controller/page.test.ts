import * as assert from 'assert';
import { app } from 'egg-mock/bootstrap';
import { mockAlphabet } from '../../lib/mock';

describe('test/app/controller/page.test.ts', () => {

  it('获取page列表', async () => {
    const result = await app.httpRequest().get('/inhouse/page/get').expect(200);
    const { body } = result
    assert(body.success === true);
  }).timeout(6000);

  it('获取page名称-列表', async () => {
    const result = await app.httpRequest().get('/inhouse/page/namelist').expect(200);
    const { body } = result
    assert(body.success === true);
  }).timeout(6000);

  it('模糊查询page', async () => {
    const result = await app.httpRequest().get('/inhouse/page/get?name=e').expect(200);
    const { body } = result
    assert(body.success === true);
  }).timeout(6000);

  it('创建page埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const page = {
      name: 'new page' + mockAlphabet(),
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }
    // 1.创建新page
    const result1: any = await app.httpRequest().post('/inhouse/page/create')
      .send(page)
    assert(result1.body.success === true);

    // 2.判断是否可以重复创建
    // const result2: any = await app.httpRequest().post('/inhouse/page/create')
    //   .send(page)
    // assert(result2.body.success === false);

  }).timeout(6000);

  it('创建page埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const page = {
      name: 'new page9',
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      error: 'err'
    }
    // 1.创建新page
    const result1: any = await app.httpRequest().post('/inhouse/page/create')
      .send(page)
    assert(result1.body.success === false);

  }).timeout(6000);

  it('更新page埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const page = {
      name: 'eric',
      description: 'update desc',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }
    // 更新page
    const id = '5f66dfc50e9fdf92aa5163dc'
    // 更新page
    const result1: any = await app.httpRequest().put(`/inhouse/page/${id}/update`)
      .send(page)
    console.log('result1:', result1.body)
    assert(result1.body.success === true);
  })

  it('更新page埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const page = {
      name: 'eric',
      description: 'update desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      aa: 'aa'
    }
    // 更新page
    const id = '5f61c5e176899e58a2af6719'
    // 更新page
    const result1: any = await app.httpRequest().put(`/inhouse/page/${id}/update`)
      .send(page)
    // console.log('result1:', result1.body)
    assert(result1.body.success === false);
  })

  it('根据id删除page埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66e69cbf153ca27f7d82c0';
    // 删除page
    const result1: any = await app.httpRequest().delete(`/inhouse/page/${id}/delete`)
    assert(result1.body.success === true);
  });

  it('根据id删除page埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66e5e4894c179e89e7e745';
    // 删除page
    const result1: any = await app.httpRequest().delete(`/inhouse/page/${id}/delete`)
    // console.log('result1:', result1.body)
    assert(result1.body.success === false);
  });

  it('根据id找page埋点', async () => {
    // app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66d2e218e79684f562ed2a'; // [已经删除]
    const result2 = await app.httpRequest().get(`/inhouse/page/${id}/detail`);
    assert(result2.body.success === true)
  });

});

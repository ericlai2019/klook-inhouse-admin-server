import * as assert from 'assert';
import { app } from 'egg-mock/bootstrap';

describe('test/app/controller/log.test.ts', () => {

  it('获取log列表', async () => {
    const result = await app.httpRequest().get('/inhouse/log/get').expect(200);
    const { body } = result
    assert(body.success === true);
  }).timeout(6000);

});

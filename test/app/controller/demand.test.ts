import * as assert from 'assert';
import { app } from 'egg-mock/bootstrap';
import { mockAlphabet } from '../../lib/mock'

describe('test/app/controller/demand.test.ts', () => {

  it('获取demand列表', async () => {
    const result = await app.httpRequest().get('/inhouse/demand/get').expect(200);
    const { body } = result
    assert(body.success === true);
  }).timeout(6000);


  it('模糊查询demand', async () => {
    const result = await app.httpRequest().get('/inhouse/demand/get?name=e').expect(200);
    const { body } = result
    // console.log('body:', body.result.list)
    assert(body.success === true);
  }).timeout(6000);

  it('创建demand埋点', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const demand = {
      name: 'new demand' + mockAlphabet(),
      description: 'desc',
      status: 'pending'
    }
    // 1.创建新demand
    const result1: any = await app.httpRequest().post('/inhouse/demand/create')
      .send(demand)
    assert(result1.body.success === true);

    // 2.判断是否可以重复创建
    // const result2: any = await app.httpRequest().post('/inhouse/demand/create')
    //   .send(demand)
    // assert(result2.body.success === false);

  }).timeout(6000);

  it('创建demand埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const demand = {
      name: 'new demand3',
      description: 'desc',
      status: 'pending',
      other: 'err'
    }
    // 1.创建新demand
    const result1: any = await app.httpRequest().post('/inhouse/demand/create')
      .send(demand)
    assert(result1.body.success === false);

  }).timeout(6000);

  it('更新demand埋点', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const demand = {
      name: 'eric',
      description: 'update desc',
      status: 'launch'
    }
    // 更新demand
    const id = '5f66ea6a5d2ebeb28dd6e2ee'
    // 更新demand
    const result1: any = await app.httpRequest().put(`/inhouse/demand/${id}/update`)
      .send(demand)
    assert(result1.body.success === true);
  })

  it('更新demand埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const demand = {
      name: 'eric',
      description: 'update desc',
      status: 'launch',
      other: 'err'
    }
    // 更新demand
    const id = '5f61bf7a9c4cda4ebb55f6a3'
    // 更新demand
    const result1: any = await app.httpRequest().put(`/inhouse/demand/${id}/update`)
      .send(demand)
    assert(result1.body.success === false);
  })

  it('根据id删除demand埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const id = '5f67052160616ec6cad9e2b2';
    // 删除demand
    const result1: any = await app.httpRequest().delete(`/inhouse/demand/${id}/delete`)
    assert(result1.body.success === true);
  });

  it('根据id找demand埋点', async () => {
    // app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66eb8505fc51b49130cc6e'; // [已经删除]
    const result2 = await app.httpRequest().get(`/inhouse/demand/${id}/detail`);
    assert(result2.body.success === true)
  });

});

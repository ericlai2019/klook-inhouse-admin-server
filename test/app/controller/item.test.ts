import * as assert from 'assert';
import { app } from 'egg-mock/bootstrap';
import { mockAlphabet } from '../../lib/mock'

describe('test/app/controller/item.test.ts', () => {

  it('获取item列表', async () => {
    const result = await app.httpRequest().get('/inhouse/item/get').expect(200);
    const { body } = result
    assert(body.success === true);
  }).timeout(6000);

  it('获取item名称-列表', async () => {
    const result = await app.httpRequest().get('/inhouse/item/namelist').expect(200);
    const { body } = result
    assert(body.code === 200);
  }).timeout(6000);

  it('模糊查询item', async () => {
    const result = await app.httpRequest().get('/inhouse/item/get?name=l').expect(200);
    const { body } = result
    // console.log('body:', body.result.list)
    assert(body.success === true);
  }).timeout(6000);

  it('创建item埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const item = {
      pageId: '5f66e6fce1fa31a33d8c7f61',
      moduleId: '5f66e835a04932a83efbc38a',
      name: 'newfffk' + mockAlphabet(),
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }
    // 1.创建新item
    const result1: any = await app.httpRequest().post('/inhouse/item/create')
      .send(item)
    assert(result1.body.code === 200);

    // 2.判断是否可以重复创建
    const result2: any = await app.httpRequest().post('/inhouse/item/create')
      .send(item)
    assert(result2.body.code === 500);

  }).timeout(6000);

  it('创建item埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const item = {
      pageId: '5f66e6fce1fa31a33d8c7f61',
      moduleId: '5f66e835a04932a83efbc38a',
      name: 'newfffk',
      description: 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      other: 'errr'
    }
    // 1.创建新item
    const result1: any = await app.httpRequest().post('/inhouse/item/create')
      .send(item)
    assert(result1.body.success === false);

  }).timeout(6000);

  it('更新item埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const item = {
      pageId: '5f66e6fce1fa31a33d8c7f61',
      moduleId: '5f66e835a04932a83efbc38a',
      name: 'new item1',
      description: 'update desc',
      objectIdType: 'NA',
      platform: {},
      extra: []
    }
    // 更新item
    const id = '5f66e8bc6ff4f2ac238484df'
    // 更新item
    const result1: any = await app.httpRequest().put(`/inhouse/item/${id}/update`)
      .send(item)
    console.log('result1:', result1.body)
    assert(result1.body.success === true);
  });

  it('更新item埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const item = {
      pageId: '5f66e6fce1fa31a33d8c7f61',
      moduleId: '5f66e835a04932a83efbc38a',
      name: 'new item1',
      description: 'update desc',
      objectIdType: 'NA',
      platform: {},
      extra: [],
      other: 'error'
    }
    // 更新item
    const id = '5f6210031d4c8f3f7ea1be0f'
    // 更新item
    const result1: any = await app.httpRequest().put(`/inhouse/item/${id}/update`)
      .send(item)
    // console.log('result1:', result1.body)
    assert(result1.body.success === false);
  })

  it('根据id删除item埋点成功', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const id = '5f670bd5390868d97e9786d6';
    // 删除item
    const result1: any = await app.httpRequest().delete(`/inhouse/item/${id}/delete`)
    console.log('result1:', result1.body)
    assert(result1.body.success === true);
  });

  it('根据id删除item埋点失败', async () => {
    app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66ea103a478bb116f942dd';
    // 删除item
    const result1: any = await app.httpRequest().delete(`/inhouse/item/${id}/delete`)
    console.log('result1:', result1.body)
    assert(result1.body.success === false);
  });

  it('根据id找item埋点', async () => {
    // app.mockCsrf(); // 如果不加会报403错误
    const id = '5f66e8d975ac83acdc70da96'; // [已经删除]
    const result2 = await app.httpRequest().get(`/inhouse/item/${id}/detail`);
    assert(result2.body.success === true)
  });


  it('获取item名称-列表', async () => {
    const result = await app.httpRequest().get('/inhouse/item/namelist').expect(200);
    const { body } = result
    assert(body.success === true);
  }).timeout(6000);

});

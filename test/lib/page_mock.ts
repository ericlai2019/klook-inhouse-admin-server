
export const generatePageRandom = function() {
  const pageName = [ 'payment', 'nearby', 'hotel', 'airport', 'popular', 'invite', 'dashboard', 'blog', 'theme', 'article', 'food' ];
  const insertDataList: any[] = []
  for (let i = 0; i < pageName.length; i++) {
    insertDataList.push({
      name: pageName[i],
      description: pageName[i] + 'desc',
      objectIdType: 'NA',
      platform: {},
      extra: []
    })
  }
  return insertDataList;
}

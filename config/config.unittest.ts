import { EggAppConfig, PowerPartial } from 'egg';

export default () => {
  const config: PowerPartial<EggAppConfig> = {};
  config.isDevelopment = true;
  config.mongoose = {
    url: 'mongodb://127.0.0.1/inhouseTest',
    options: { useUnifiedTopology: true }
    // mongoose global plugins, expected a function or an array of function and options
    // plugins: [ createdPlugin, [ updatedPlugin, pluginOptions ]],
  };
  return config;
};

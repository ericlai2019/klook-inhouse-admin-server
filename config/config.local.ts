import { EggAppConfig, PowerPartial } from 'egg';

export default () => {
  const config: PowerPartial<EggAppConfig> = {};
  config.isDevelopment = true;
  config.mongoose = {
    url: 'mongodb://127.0.0.1:27017/inhouse',
    options: { useUnifiedTopology: true }
  };
  config.security = {
    csrf: {
      enable: false
    }
  }
  return config;
};

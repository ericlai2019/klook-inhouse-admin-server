// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
type AnyClass = new (...args: any[]) => any;
type AnyFunc<T = any> = (...args: any[]) => T;
type CanExportFunc = AnyFunc<Promise<any>> | AnyFunc<IterableIterator<any>>;
type AutoInstanceType<T, U = T extends CanExportFunc ? T : T extends AnyFunc ? ReturnType<T> : T> = U extends AnyClass ? InstanceType<U> : U;
import ExportTest from '../../../app/service/Test';
import ExportDemand from '../../../app/service/demand';
import ExportDemandSpm from '../../../app/service/demandSpm';
import ExportItem from '../../../app/service/item';
import ExportLog from '../../../app/service/log';
import ExportModule from '../../../app/service/module';
import ExportPage from '../../../app/service/page';

declare module 'egg' {
  interface IService {
    test: AutoInstanceType<typeof ExportTest>;
    demand: AutoInstanceType<typeof ExportDemand>;
    demandSpm: AutoInstanceType<typeof ExportDemandSpm>;
    item: AutoInstanceType<typeof ExportItem>;
    log: AutoInstanceType<typeof ExportLog>;
    module: AutoInstanceType<typeof ExportModule>;
    page: AutoInstanceType<typeof ExportPage>;
  }
}

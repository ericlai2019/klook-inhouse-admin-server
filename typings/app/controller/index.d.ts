// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportDemand from '../../../app/controller/demand';
import ExportDemandSpm from '../../../app/controller/demandSpm';
import ExportHome from '../../../app/controller/home';
import ExportItem from '../../../app/controller/item';
import ExportLog from '../../../app/controller/log';
import ExportModule from '../../../app/controller/module';
import ExportPage from '../../../app/controller/page';
import ExportJsonSchemaDemand from '../../../app/controller/json-schema/demand';
import ExportJsonSchemaDemandSpm from '../../../app/controller/json-schema/demandSpm';
import ExportJsonSchemaItem from '../../../app/controller/json-schema/item';
import ExportJsonSchemaModule from '../../../app/controller/json-schema/module';
import ExportJsonSchemaPage from '../../../app/controller/json-schema/page';
import ExportValidatorDemand from '../../../app/controller/validator/demand';
import ExportValidatorDemandSpm from '../../../app/controller/validator/demandSpm';
import ExportValidatorItem from '../../../app/controller/validator/item';
import ExportValidatorModule from '../../../app/controller/validator/module';
import ExportValidatorPage from '../../../app/controller/validator/page';

declare module 'egg' {
  interface IController {
    demand: ExportDemand;
    demandSpm: ExportDemandSpm;
    home: ExportHome;
    item: ExportItem;
    log: ExportLog;
    module: ExportModule;
    page: ExportPage;
    jsonSchema: {
      demand: ExportJsonSchemaDemand;
      demandSpm: ExportJsonSchemaDemandSpm;
      item: ExportJsonSchemaItem;
      module: ExportJsonSchemaModule;
      page: ExportJsonSchemaPage;
    }
    validator: {
      demand: ExportValidatorDemand;
      demandSpm: ExportValidatorDemandSpm;
      item: ExportValidatorItem;
      module: ExportValidatorModule;
      page: ExportValidatorPage;
    }
  }
}

// This file is created by egg-ts-helper@1.25.8
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportDemand from '../../../app/model/demand';
import ExportDemandSpm from '../../../app/model/demandSpm';
import ExportItem from '../../../app/model/item';
import ExportLog from '../../../app/model/log';
import ExportModule from '../../../app/model/module';
import ExportPage from '../../../app/model/page';
import ExportSchemaDemand from '../../../app/model/schema/demand';
import ExportSchemaDemandSpm from '../../../app/model/schema/demandSpm';
import ExportSchemaItem from '../../../app/model/schema/item';
import ExportSchemaLog from '../../../app/model/schema/log';
import ExportSchemaModule from '../../../app/model/schema/module';
import ExportSchemaPage from '../../../app/model/schema/page';
import ExportSchemaSpm from '../../../app/model/schema/spm';

declare module 'egg' {
  interface IModel {
    Demand: ReturnType<typeof ExportDemand>;
    DemandSpm: ReturnType<typeof ExportDemandSpm>;
    Item: ReturnType<typeof ExportItem>;
    Log: ReturnType<typeof ExportLog>;
    Module: ReturnType<typeof ExportModule>;
    Page: ReturnType<typeof ExportPage>;
    Schema: {
      Demand: ReturnType<typeof ExportSchemaDemand>;
      DemandSpm: ReturnType<typeof ExportSchemaDemandSpm>;
      Item: ReturnType<typeof ExportSchemaItem>;
      Log: ReturnType<typeof ExportSchemaLog>;
      Module: ReturnType<typeof ExportSchemaModule>;
      Page: ReturnType<typeof ExportSchemaPage>;
      Spm: ReturnType<typeof ExportSchemaSpm>;
    }
  }
}
